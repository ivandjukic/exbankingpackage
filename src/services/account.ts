import { AccountRepositoryInterface } from '../repository/account';
import {
  Account,
  BankingErrors,
  CurrencyBalance,
  CurrencyDoesNotExistsError,
  DepositResponse,
  TransferMoneyResponse,
  UserDoesNotExistError,
  WithdrawalResponse,
} from '../types';
import { UserService } from './user';
import UserRepository from '../repository/user';
import { checkDoesCurrencyExists } from '../utils/checkDoesCurrencyExists';

export interface AccountServiceInterface {
  getAll(): Account[];
  deposit(username: string, amount: number, currency: string): DepositResponse;
  withdraw(username: string, amount: number, currency: string): WithdrawalResponse;
  getBalance(username: string): CurrencyBalance[] | UserDoesNotExistError;
  getBalanceByCurrencyName(
    username: string,
    currency: string
  ): { balance: number } | UserDoesNotExistError | CurrencyDoesNotExistsError;
  send(
    fromUsername: string,
    toUsername: string,
    amount: number,
    currency: string
  ): TransferMoneyResponse;
}

export class AccountService implements AccountServiceInterface {
  private repository: AccountRepositoryInterface;

  constructor(repository: AccountRepositoryInterface) {
    this.repository = repository;
  }

  public getAll(): Account[] {
    return this.repository.getAll();
  }

  public deposit(username: string, amount: number, currency: string): DepositResponse {
    const userService = new UserService(UserRepository);
    const user = userService.findByUsername(username);
    if (!user) {
      return {
        success: false,
        message: BankingErrors.UserDoesNotExist,
      };
    }
    if (!checkDoesCurrencyExists(currency.toUpperCase())) {
      return {
        success: false,
        message: BankingErrors.CurrencyDoesNotExists,
      };
    }
    if (amount <= 0) {
      return {
        success: false,
        message: BankingErrors.WrongArguments,
      };
    }
    this.repository.deposit(user.id, amount, currency.toUpperCase());
    const account = this.repository.findByUserId(user.id);
    return {
      success: true,
      balance: account?.balances ?? [],
    };
  }

  public withdraw(username: string, amount: number, currency: string): WithdrawalResponse {
    const userService = new UserService(UserRepository);
    const user = userService.findByUsername(username);
    if (!user) {
      return {
        success: false,
        message: BankingErrors.UserDoesNotExist,
      };
    }
    if (!checkDoesCurrencyExists(currency)) {
      return {
        success: false,
        message: BankingErrors.CurrencyDoesNotExists,
      };
    }
    if (amount <= 0) {
      return {
        success: false,
        message: BankingErrors.WrongArguments,
      };
    }
    const balance = this.repository.getBalanceByUserIdAndCurrency(user.id, currency.toUpperCase());
    if (balance < amount) {
      return {
        success: false,
        message: BankingErrors.NotEnoughMoney,
      };
    }
    this.repository.withdraw(user.id, amount, currency.toUpperCase());
    const account = this.repository.findByUserId(user.id);
    return {
      success: true,
      balance: account?.balances ?? [],
    };
  }

  public getBalance(username: string): CurrencyBalance[] | UserDoesNotExistError {
    const userService = new UserService(UserRepository);
    const user = userService.findByUsername(username);
    if (!user) {
      return {
        success: false,
        message: BankingErrors.UserDoesNotExist,
      };
    }
    const account = this.repository.findByUserId(user.id);
    return account?.balances ?? [];
  }

  public getBalanceByCurrencyName(
    username: string,
    currency: string
  ): { balance: number } | UserDoesNotExistError | CurrencyDoesNotExistsError {
    const userService = new UserService(UserRepository);
    const user = userService.findByUsername(username);
    if (!user) {
      return {
        success: false,
        message: BankingErrors.UserDoesNotExist,
      };
    }
    if (!checkDoesCurrencyExists(currency)) {
      return {
        success: false,
        message: BankingErrors.CurrencyDoesNotExists,
      };
    }
    const balance = this.repository.getBalanceByUserIdAndCurrency(user.id, currency.toUpperCase());
    return { balance };
  }

  public send(
    fromUsername: string,
    toUsername: string,
    amount: number,
    currency: string
  ): TransferMoneyResponse {
    const userService = new UserService(UserRepository);
    const sender = userService.findByUsername(fromUsername);
    if (!sender) {
      return {
        success: false,
        message: BankingErrors.SenderDoesNotExists,
      };
    }
    const receiver = userService.findByUsername(toUsername);
    if (!receiver) {
      return {
        success: false,
        message: BankingErrors.ReceiverDoesNotExist,
      };
    }
    if (!checkDoesCurrencyExists(currency.toUpperCase())) {
      return {
        success: false,
        message: BankingErrors.CurrencyDoesNotExists,
      };
    }
    const balance = this.repository.getBalanceByUserIdAndCurrency(
      sender.id,
      currency.toUpperCase()
    );
    if (balance < amount) {
      return {
        success: false,
        message: BankingErrors.NotEnoughMoney,
      };
    }
    this.repository.withdraw(sender.id, amount, currency.toUpperCase());
    this.repository.deposit(receiver.id, amount, currency.toUpperCase());
    const senderAccount = this.repository.findByUserId(sender.id);
    const receiverAccount = this.repository.findByUserId(receiver.id);
    return {
      success: true,
      senderBalance: senderAccount?.balances ?? [],
      receiverBalance: receiverAccount?.balances ?? [],
    };
  }
}
