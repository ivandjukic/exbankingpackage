import UserRepository from '../../repository/user';
import AccountRepository from '../../repository/account';
import { AccountService } from '../account';
import {
  Account,
  CurrencyDoesNotExistsError,
  NotEnoughMoney,
  ReceiverDoesNotExist,
  SenderDoesNotExists,
  UserDoesNotExistError,
  WrongArguments,
} from '../../types';
import { checkDoesCurrencyExists } from '../../utils/checkDoesCurrencyExists';

jest.mock('../../repository/user');
jest.mock('../../repository/account');
jest.mock('../../utils/checkDoesCurrencyExists');

describe('UserService', () => {
  let mockAccountRepository: jest.Mocked<typeof AccountRepository>;
  let mockUserRepository: jest.Mocked<typeof UserRepository>;
  let mockCheckDoesCurrencyExists;
  let accountService: AccountService;
  const mockUser1 = { id: 'mock-user-id-1', username: 'mock-user-username-1' };
  const mockUser2 = { id: 'mock-user-id-2', username: 'mock-user-username-2' };
  const mockAccount1: Account = {
    id: 'mock-id-1',
    userId: 'mock-user-id-1',
    balances: [],
  };
  const mockAccount2: Account = {
    id: 'mock-id-2',
    userId: 'mock-user-id-2',
    balances: [
      { currencyName: 'EUR', balance: 500 },
      { currencyName: 'USD', balance: 1000 },
    ],
  };

  beforeEach(() => {
    mockAccountRepository = AccountRepository as jest.Mocked<typeof AccountRepository>;
    mockUserRepository = UserRepository as jest.Mocked<typeof UserRepository>;
    mockCheckDoesCurrencyExists = checkDoesCurrencyExists;
    accountService = new AccountService(mockAccountRepository);
  });

  describe('getAll', () => {
    it('should return all accounts', () => {
      mockAccountRepository.getAll.mockImplementationOnce(() => [mockAccount1, mockAccount2]);
      const result = accountService.getAll();
      expect(mockAccountRepository.getAll).toHaveBeenCalled();
      expect(result).toEqual([mockAccount1, mockAccount2]);
    });

    it('should return empty response if there is no accounts', () => {
      mockAccountRepository.getAll.mockImplementationOnce(() => []);
      const result = accountService.getAll();
      expect(mockAccountRepository.getAll).toHaveBeenCalled();
      expect(result).toEqual([]);
    });
  });

  describe('getBalance', () => {
    it('should return an error if user does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      const response = accountService.getBalance(mockUser1.username);
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(UserDoesNotExistError.check(response)).toBeTruthy();
      expect(mockAccountRepository.findByUserId).not.toHaveBeenCalled();
    });
    it('should return empty array if user does not have any balance', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockAccountRepository.findByUserId.mockImplementationOnce(() => mockAccount1);
      const response = accountService.getBalance(mockUser1.username);
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(mockAccountRepository.findByUserId).toHaveBeenCalledWith(mockUser1.id);
      expect(response).toEqual([]);
    });
    it('should return balance', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockAccountRepository.findByUserId.mockImplementationOnce(() => mockAccount2);
      const response = accountService.getBalance(mockUser1.username);
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(mockAccountRepository.findByUserId).toHaveBeenCalledWith(mockUser1.id);
      expect(response).toEqual(mockAccount2.balances);
    });
  });

  describe('getBalanceByCurrencyName', () => {
    it('should return an error if user does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      const response = accountService.getBalanceByCurrencyName(mockUser1.username, 'eur');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(UserDoesNotExistError.check(response)).toBeTruthy();
      expect(mockAccountRepository.getBalanceByUserIdAndCurrency).not.toHaveBeenCalled();
      expect(mockCheckDoesCurrencyExists).not.toHaveBeenCalled();
    });

    it('should return an error if currency does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => false);
      const response = accountService.getBalanceByCurrencyName(mockUser1.username, 'BTC');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(CurrencyDoesNotExistsError.check(response)).toBeTruthy();
      expect(mockCheckDoesCurrencyExists).toHaveBeenCalledWith('BTC');
      expect(mockAccountRepository.getBalanceByUserIdAndCurrency).not.toHaveBeenCalled();
    });

    it('should return 0 if user does not have balance for the given currency', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => true);
      mockAccountRepository.getBalanceByUserIdAndCurrency.mockImplementationOnce(() => 0);
      const response = accountService.getBalanceByCurrencyName(mockUser1.username, 'EUR');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(mockAccountRepository.getBalanceByUserIdAndCurrency).toHaveBeenCalledWith(
        mockUser1.id,
        'EUR'
      );
      expect(response).toEqual({ balance: 0 });
    });

    it('should return balance', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => true);
      mockAccountRepository.getBalanceByUserIdAndCurrency.mockImplementationOnce(() => 100);
      const response = accountService.getBalanceByCurrencyName(mockUser1.username, 'EUR');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(mockAccountRepository.getBalanceByUserIdAndCurrency).toHaveBeenCalledWith(
        mockUser1.id,
        'EUR'
      );
      expect(response).toEqual({ balance: 100 });
    });
  });

  describe('deposit', () => {
    it('should return an error if user does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      const response = accountService.deposit(mockUser1.username, 100, 'eur');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(UserDoesNotExistError.check(response)).toBeTruthy();
      expect(mockAccountRepository.deposit).not.toHaveBeenCalled();
      expect(mockCheckDoesCurrencyExists).not.toHaveBeenCalled();
    });

    it('should return an error if currency does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => false);
      const response = accountService.deposit(mockUser1.username, 10, 'BTC');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(CurrencyDoesNotExistsError.check(response)).toBeTruthy();
      expect(mockCheckDoesCurrencyExists).toHaveBeenCalledWith('BTC');
      expect(mockAccountRepository.deposit).not.toHaveBeenCalled();
    });

    it('should return an error if amount is equal to 0', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => true);
      const response = accountService.deposit(mockUser1.username, 0, 'EUR');
      expect(WrongArguments.check(response)).toBeTruthy();
    });

    it('should return an error if amount is less than 0', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => true);
      const response = accountService.deposit(mockUser1.username, -100, 'EUR');
      expect(WrongArguments.check(response)).toBeTruthy();
    });

    it('should deposit funds', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => true);
      mockAccountRepository.findByUserId.mockImplementationOnce(() => mockAccount2);
      accountService.deposit(mockUser1.username, 100, 'CHF');
      expect(mockAccountRepository.deposit).toHaveBeenCalledWith(mockUser1.id, 100, 'CHF');
      expect(mockAccountRepository.findByUserId).toHaveBeenCalledWith(mockUser1.id);
    });
  });

  describe('withdraw', () => {
    it('should return an error if user does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      const response = accountService.withdraw(mockUser1.username, 100, 'eur');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(UserDoesNotExistError.check(response)).toBeTruthy();
      expect(mockAccountRepository.withdraw).not.toHaveBeenCalled();
      expect(mockCheckDoesCurrencyExists).not.toHaveBeenCalled();
    });

    it('should return an error if currency does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => false);
      const response = accountService.withdraw(mockUser1.username, 10, 'BTC');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(CurrencyDoesNotExistsError.check(response)).toBeTruthy();
      expect(mockCheckDoesCurrencyExists).toHaveBeenCalledWith('BTC');
      expect(mockAccountRepository.withdraw).not.toHaveBeenCalled();
    });

    it('should return error if user does not have enough funds', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => true);
      mockAccountRepository.getBalanceByUserIdAndCurrency.mockImplementationOnce(() => 10);
      const response = accountService.withdraw(mockUser1.username, 20, 'EUR');
      expect(NotEnoughMoney.check(response)).toBeTruthy();
      expect(mockAccountRepository.getBalanceByUserIdAndCurrency).toHaveBeenCalledWith(
        mockUser1.id,
        'EUR'
      );
      expect(mockAccountRepository.withdraw).not.toHaveBeenCalled();
    });

    it('should withdraw funds', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => true);
      mockAccountRepository.getBalanceByUserIdAndCurrency.mockImplementationOnce(() => 100);
      accountService.withdraw(mockUser1.username, 100, 'CHF');
      expect(mockAccountRepository.withdraw).toHaveBeenCalledWith(mockUser1.id, 100, 'CHF');
      expect(mockAccountRepository.findByUserId).toHaveBeenCalledWith(mockUser1.id);
    });
  });

  describe('send', () => {
    it('should return an error if sender does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      const response = accountService.send(mockUser1.username, mockUser2.username, 100, 'eur');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(SenderDoesNotExists.check(response)).toBeTruthy();
      expect(mockAccountRepository.withdraw).not.toHaveBeenCalled();
      expect(mockAccountRepository.deposit).not.toHaveBeenCalled();
    });

    it('should return an error if receiver does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      const response = accountService.send(mockUser1.username, mockUser2.username, 100, 'eur');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser2.username);
      expect(ReceiverDoesNotExist.check(response)).toBeTruthy();
      expect(mockAccountRepository.withdraw).not.toHaveBeenCalled();
      expect(mockAccountRepository.deposit).not.toHaveBeenCalled();
    });

    it('should return an error if currency does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser2);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => false);
      const response = accountService.send(mockUser1.username, mockUser2.username, 10, 'BTC');
      expect(CurrencyDoesNotExistsError.check(response)).toBeTruthy();
      expect(mockCheckDoesCurrencyExists).toHaveBeenCalledWith('BTC');
      expect(mockAccountRepository.withdraw).not.toHaveBeenCalled();
      expect(mockAccountRepository.deposit).not.toHaveBeenCalled();
    });

    it('should return error if sender user does not have enough funds', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser2);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => true);
      mockAccountRepository.getBalanceByUserIdAndCurrency.mockImplementationOnce(() => 10);
      const response = accountService.send(mockUser1.username, mockUser2.username, 100, 'EUR');
      expect(NotEnoughMoney.check(response)).toBeTruthy();
      expect(mockAccountRepository.getBalanceByUserIdAndCurrency).toHaveBeenCalledWith(
        mockUser1.id,
        'EUR'
      );
      expect(mockAccountRepository.withdraw).not.toHaveBeenCalled();
      expect(mockAccountRepository.deposit).not.toHaveBeenCalled();
    });

    it('should transfer funds', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser2);
      mockCheckDoesCurrencyExists.mockImplementationOnce(() => true);
      mockAccountRepository.getBalanceByUserIdAndCurrency.mockImplementationOnce(() => 200);
      accountService.send(mockUser1.username, mockUser2.username, 100, 'CHF');
      expect(mockAccountRepository.withdraw).toHaveBeenCalledWith(mockUser1.id, 100, 'CHF');
      expect(mockAccountRepository.deposit).toHaveBeenCalledWith(mockUser2.id, 100, 'CHF');
      expect(mockAccountRepository.findByUserId).toHaveBeenCalledWith(mockUser1.id);
      expect(mockAccountRepository.findByUserId).toHaveBeenCalledWith(mockUser2.id);
    });
  });
});
