import UserRepository from '../../repository/user';
import AccountRepository from '../../repository/account';
import { UserService, UserServiceInterface } from '../user';
import { ResponseOk, UserAlreadyExistsError } from '../../types';

jest.mock('../../repository/user');
jest.mock('../../repository/account');

describe('UserService', () => {
  let mockUserRepository: jest.Mocked<typeof UserRepository>;
  let mockAccountRepository: jest.Mocked<typeof AccountRepository>;
  let userService: UserServiceInterface;
  const mockUser1 = { id: 'mock-user-id-1', username: 'mock-user-username-1' };
  const mockUser2 = { id: 'mock-user-id-2', username: 'mock-user-username-2' };

  beforeEach(() => {
    mockUserRepository = UserRepository as jest.Mocked<typeof UserRepository>;
    mockAccountRepository = AccountRepository as jest.Mocked<typeof AccountRepository>;
    userService = new UserService(mockUserRepository);
  });

  describe('createUser', () => {
    it('should call doesUserExists', () => {
      mockUserRepository.create.mockImplementationOnce(() => mockUser1);
      userService.createUser(mockUser1.username);
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
    });

    it('should return error if username already exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      const response = userService.createUser('some-name');
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith('some-name');
      expect(mockUserRepository.create).not.toHaveBeenCalled();
      expect(UserAlreadyExistsError.check(response)).toBeTruthy();
    });

    it('should call create user repository function', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      mockUserRepository.create.mockImplementationOnce(() => mockUser1);
      const response = userService.createUser(mockUser1.username);
      expect(ResponseOk.check(response)).toBeTruthy();
      expect(mockUserRepository.create).toHaveBeenCalledWith(mockUser1.username);
    });

    it('should call create account', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      mockUserRepository.create.mockImplementationOnce(() => mockUser1);
      const response = userService.createUser(mockUser1.username);
      expect(ResponseOk.check(response)).toBeTruthy();
      expect(mockAccountRepository.createAccount).toHaveBeenCalledWith(mockUser1.id);
    });
  });

  describe('getAll', () => {
    it('should call getAll repo function', () => {
      userService.getAll();
      expect(mockUserRepository.getAll).toHaveBeenCalled();
    });

    it('should return response', () => {
      mockUserRepository.getAll.mockImplementationOnce(() => [mockUser1, mockUser2]);
      const result = userService.getAll();
      expect(result).toEqual([mockUser1, mockUser2]);
      expect(mockUserRepository.getAll).toHaveBeenCalled();
    });
  });

  describe('doesUserExists', () => {
    it('should return false if user does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      const result = userService.doesUserExists(mockUser1.username);
      expect(result).toEqual(false);
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
    });

    it('should return true when user exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser1);
      const result = userService.doesUserExists(mockUser1.username);
      expect(result).toEqual(true);
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
    });
  });

  describe('findByUsername', () => {
    it('should return user when user exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => mockUser2);
      const result = userService.findByUsername(mockUser2.username);
      expect(result).toEqual(mockUser2);
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser2.username);
    });

    it('should return null when user does not exists', () => {
      mockUserRepository.findByUsername.mockImplementationOnce(() => null);
      const result = userService.findByUsername(mockUser1.username);
      expect(result).toEqual(null);
      expect(mockUserRepository.findByUsername).toHaveBeenCalledWith(mockUser1.username);
    });
  });
});
