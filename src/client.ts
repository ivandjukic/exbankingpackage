import { UserService, UserServiceInterface } from './services/user';
import { AccountService, AccountServiceInterface } from './services/account';
import UserRepository from './repository/user';
import AccountRepository from './repository/account';

interface ClientInstance {
  User: UserServiceInterface;
  Account: AccountServiceInterface;
}
export class Client implements ClientInstance {
  public User: UserService;

  public Account: AccountService;

  constructor() {
    this.User = new UserService(UserRepository);
    this.Account = new AccountService(AccountRepository);
  }
}
