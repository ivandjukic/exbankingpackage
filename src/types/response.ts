import { Array, Literal, Record, Static, Union } from 'runtypes';
import { CurrencyBalance } from './account';

export const ResponseOk = Record({
  success: Literal(true),
});
export type ResponseOk = Static<typeof ResponseOk>;

export const ResponseError = Record({
  success: Literal(false),
});
export type ResponseError = Static<typeof ResponseError>;

export enum BankingErrors {
  UserAlreadyExists = 'UserAlreadyExists',
  UserDoesNotExist = 'UserDoesNotExist',
  CurrencyDoesNotExists = 'CurrencyDoesNotExists',
  WrongArguments = 'WrongArguments',
  NotEnoughMoney = 'NotEnoughMoney',
  SenderDoesNotExists = 'SenderDoesNotExists',
  ReceiverDoesNotExist = 'ReceiverDoesNotExist',
}

export const UserAlreadyExistsError = ResponseError.And(
  Record({
    message: Literal(BankingErrors.UserAlreadyExists),
  })
);

export type UserAlreadyExistsError = Static<typeof UserAlreadyExistsError>;

export const UserDoesNotExistError = ResponseError.And(
  Record({
    message: Literal(BankingErrors.UserDoesNotExist),
  })
);

export type UserDoesNotExistError = Static<typeof UserDoesNotExistError>;

export const CurrencyDoesNotExistsError = ResponseError.And(
  Record({
    message: Literal(BankingErrors.CurrencyDoesNotExists),
  })
);

export type CurrencyDoesNotExistsError = Static<typeof CurrencyDoesNotExistsError>;

export const WrongArguments = ResponseError.And(
  Record({
    message: Literal(BankingErrors.WrongArguments),
  })
);

export type WrongArguments = Static<typeof WrongArguments>;

export const NotEnoughMoney = ResponseError.And(
  Record({
    message: Literal(BankingErrors.NotEnoughMoney),
  })
);

export type NotEnoughMoney = Static<typeof NotEnoughMoney>;

export const DepositResponse = Union(
  WrongArguments,
  UserDoesNotExistError,
  CurrencyDoesNotExistsError,
  ResponseOk.And(
    Record({
      balance: Array(CurrencyBalance),
    })
  )
);

export type DepositResponse = Static<typeof DepositResponse>;

export const WithdrawalResponse = Union(
  WrongArguments,
  UserDoesNotExistError,
  CurrencyDoesNotExistsError,
  NotEnoughMoney,
  ResponseOk.And(
    Record({
      balance: Array(CurrencyBalance),
    })
  )
);

export type WithdrawalResponse = Static<typeof WithdrawalResponse>;

export const SenderDoesNotExists = ResponseError.And(
  Record({
    message: Literal(BankingErrors.SenderDoesNotExists),
  })
);

export type SenderDoesNotExists = Static<typeof SenderDoesNotExists>;

export const ReceiverDoesNotExist = ResponseError.And(
  Record({
    message: Literal(BankingErrors.ReceiverDoesNotExist),
  })
);

export type ReceiverDoesNotExist = Static<typeof ReceiverDoesNotExist>;

export const TransferMoneyResponse = Union(
  ResponseOk.And(
    Record({
      senderBalance: Array(CurrencyBalance),
      receiverBalance: Array(CurrencyBalance),
    })
  ),
  SenderDoesNotExists,
  ReceiverDoesNotExist,
  CurrencyDoesNotExistsError,
  NotEnoughMoney
);

export type TransferMoneyResponse = Static<typeof TransferMoneyResponse>;
