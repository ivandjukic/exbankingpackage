import { Record, Static, String } from 'runtypes';

export const CreateUserPayload = Record({
  username: String,
});

export type CreateUserPayload = Static<typeof CreateUserPayload>;

export const User = Record({
  id: String,
  username: String,
});

export type User = Static<typeof User>;
