import { Array, Record, Static, String, Number } from 'runtypes';

export const CurrencyBalance = Record({
  currencyName: String,
  balance: Number,
});
export type CurrencyBalance = Static<typeof CurrencyBalance>;

export const Account = Record({
  id: String,
  userId: String,
  balances: Array(CurrencyBalance),
});

export type Account = Static<typeof Account>;
