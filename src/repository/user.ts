import { v4 as uuid } from 'uuid';
import { User } from '../types';

export interface UserRepositoryInterface {
  create(username: string): User;
  findById(id: string): User | null;
  findByUsername(username: string): User | null;
  getAll(): User[];
}

class UserRepository implements UserRepositoryInterface {
  private users: User[];

  constructor() {
    this.users = [];
  }

  public create(username: string): User {
    const user: User = {
      id: uuid(),
      username,
    };
    this.users.push(user);
    return user;
  }

  public findById(id: string): User | null {
    return this.users.find((user) => user.id === id) ?? null;
  }

  public findByUsername(username: string): User | null {
    return this.users.find((user) => user.username === username) ?? null;
  }

  public getAll(): User[] {
    return this.users;
  }
}

export default new UserRepository();
