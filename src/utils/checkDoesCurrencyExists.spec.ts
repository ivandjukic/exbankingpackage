import { checkDoesCurrencyExists } from './checkDoesCurrencyExists';

describe('checkDoesCurrencyExists', () => {
  it('should return true for the existing currencies', () => {
    expect(checkDoesCurrencyExists('eur')).toBeTruthy();
    expect(checkDoesCurrencyExists('rsd')).toBeTruthy();
    expect(checkDoesCurrencyExists('eur')).toBeTruthy();
    expect(checkDoesCurrencyExists('inr')).toBeTruthy();
    expect(checkDoesCurrencyExists('sar')).toBeTruthy();
    expect(checkDoesCurrencyExists('gbp')).toBeTruthy();
  });

  it('should return true for the existing currencies written in UPPERCASE', () => {
    expect(checkDoesCurrencyExists('EUR')).toBeTruthy();
    expect(checkDoesCurrencyExists('RSD')).toBeTruthy();
    expect(checkDoesCurrencyExists('USD')).toBeTruthy();
  });

  it('should return false for the non existing currencies', () => {
    expect(checkDoesCurrencyExists('ASDS')).toBeFalsy();
    expect(checkDoesCurrencyExists('BTC')).toBeFalsy();
    expect(checkDoesCurrencyExists('RTH')).toBeFalsy();
    expect(checkDoesCurrencyExists('USA')).toBeFalsy();
    expect(checkDoesCurrencyExists('ETH')).toBeFalsy();
    expect(checkDoesCurrencyExists('DOGE')).toBeFalsy();
  });
});
