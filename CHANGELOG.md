# CHANGELOG

## Version 1.0.0

- Initial package setup

## Version 1.0.1

- Fix package.json

## Version 1.0.2

- Fix package.json

## Version 1.0.4

- Export types
