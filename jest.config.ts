module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    moduleDirectories: ['node_modules', 'src'],
    clearMocks: true,
    testPathIgnorePatterns: ['<rootDir>/node_modules', '<rootDir>/dist', '<rootDir>/testing'],
    collectCoverageFrom: ['src/**/*.{js,ts}', '!**/node_modules/**', '!**/testing/**'],
    coverageReporters: ['text-summary', 'lcov', 'cobertura'],
};